#!/bin/bash

function join { local IFS="$1"; shift; echo "$*"; }

echo "************************************"
echo "**    RUN DJANGO PYTHON SCRIPT    **"
echo "**                         v1.0.0 **"
echo "************************************"

# Split directory script
IFS='/' read -ra MLP <<< "$1"
PY_SCRIPT=${MLP[-1]}

unset MLP[${#MLP[@]}-1]

IFS='.' read -ra MODULE <<< "$PY_SCRIPT"

FROM=$(join . ${MLP[@]})

# Check if user set parameter(s) 
if [ -z "$2" ]

    then
        COMMAND="from $FROM import $MODULE"
    else
        declare -a PARAMS
        i=0
        for p in "$@"
            do
              if [[ "$i" -gt 1 ]]; then
                re='^[+-]?[0-9]+([.][0-9]+)?$'
                if ! [[ $p =~ $re ]]
                    then
                        PARAMS+=("'$p'")
                    else
                        PARAMS+=($p)
                fi
              fi
              ((i=i+1))
            done

            KARGS=$(join , ${PARAMS[@]})
            FUNCTION="$MODULE.$2($KARGS)"
            echo "FUNCTION > $FUNCTION"
            COMMAND="from $FROM import $MODULE; $FUNCTION"
        
fi

python3 manage.py shell --command="$COMMAND"